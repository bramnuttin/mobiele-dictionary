﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Provider;
using Android.Views;
using Android.Widget;

namespace Dictionary
{
	public class DefinitionsAdapter : BaseAdapter<Dictionary.services.aonaware.com.Definition>
	{
		private Dictionary.services.aonaware.com.Definition[] items;
		private Context context;

		public DefinitionsAdapter (Context context, Dictionary.services.aonaware.com.Definition[] items)
		{
			this.items = items;
			this.context = context;
		}

		public override int Count {
			get {
				return items.Length;
			}
		}
		public override long GetItemId (int position)
		{
			return position;
		}
		public override Dictionary.services.aonaware.com.Definition this [int index] {
			get {
				return items [index];
			}
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View row = convertView;

			if (row == null) {
				row = LayoutInflater.From (context).Inflate (Resource.Layout.DefinitionListView_row, null, false);
			}

			TextView dictionaryText = row.FindViewById<TextView> (Resource.Id.definitionDictionaryText);
			TextView definitionText = row.FindViewById<TextView> (Resource.Id.definitionText);
			dictionaryText.Text = items [position].Dictionary.Name;
			definitionText.Text = items [position].WordDefinition;
			return row;
		}




	}
}

