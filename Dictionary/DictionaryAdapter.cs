﻿using System;
using Android.Widget;
using Android.Content;
using Android.Views;
using System.Collections.Generic;

namespace Dictionary
{
	public class DictionaryAdapter : BaseAdapter<Dictionary.services.aonaware.com.Dictionary>
	{
		private Dictionary.services.aonaware.com.Dictionary[] items;
		private Context context;
		private List<string> allowedDictionaries;

		public DictionaryAdapter (Context context, Dictionary.services.aonaware.com.Dictionary[] items, List<string> allowedDictionaries)
		{
			this.items = items;
			this.context = context;
			this.allowedDictionaries = allowedDictionaries;
			Console.WriteLine ("items length: " + items.Length + Count);
		}

		public override int Count {
			get {
				return items.Length;
			}
		}
		public override long GetItemId (int position)
		{
			return position;
		}
		public override Dictionary.services.aonaware.com.Dictionary this [int index] {
			get {
				return items [index];
			}
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			

			View row = convertView;

			if (row == null) {
				row = LayoutInflater.From (context).Inflate (Resource.Layout.DictionaryListView_row, null, false);
			}
			CheckBox dictionaryCheckBox = row.FindViewById<CheckBox> (Resource.Id.dictionaryCheckBox);
			dictionaryCheckBox.Text = items [position].Name;
			dictionaryCheckBox.SetTag (Resource.Id.dictionaryCheckBox, items [position].Id);
			if (allowedDictionaries == null) {
				Console.WriteLine ("allowedDictionaries == null");
			}
			if (items[position] == null) {
				Console.WriteLine ("items[position] == null");
			}
			if (allowedDictionaries.Contains (items [position].Id)) {
				dictionaryCheckBox.Checked = true;
			}
			dictionaryCheckBox.CheckedChange += DictionaryCheckBox_CheckedChange;
			return row;
		}

		void DictionaryCheckBox_CheckedChange (object sender, CompoundButton.CheckedChangeEventArgs e)
		{
			string id= (string)((CheckBox)sender).GetTag (Resource.Id.dictionaryCheckBox);
			if (((CheckBox)sender).Checked) {
				allowedDictionaries.Add (id);
			} else {
				allowedDictionaries.Remove (id);
			}
		}

	}
}

