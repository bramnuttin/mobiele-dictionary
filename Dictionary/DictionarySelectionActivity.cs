﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Dictionary
{
	[Activity (Label = "DictionarySelection")]			
	public class DictionarySelectionActivity : Activity
	{
		private Button save;
		private Button cancel;
		private ListView dictionaryListView;
		private DictionaryAdapter dictionaryAdapter;
		private List<string> allowedDictionaries;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.DictionarySelection);

			save = FindViewById<Button> (Resource.Id.dictionarySelectionSave);
			cancel = FindViewById<Button> (Resource.Id.dictionarySelectionCancel);
			dictionaryListView = FindViewById<ListView> (Resource.Id.dictionaryListView);
			allowedDictionaries = new List<string> (this.Intent.GetStringArrayListExtra ("allowedDictionaries"));// as List<string>;

			var service = new services.aonaware.com.DictService ();
			dictionaryAdapter = new DictionaryAdapter (this, service.DictionaryList(), allowedDictionaries);
			dictionaryListView.Adapter = dictionaryAdapter;

			save.Click += Save_Click;
			cancel.Click += Cancel_Click;
		}

		void Cancel_Click (object sender, EventArgs e)
		{
			Finish ();
		}

		void Save_Click (object sender, EventArgs e)
		{
			Intent data = new Intent();
			data.PutStringArrayListExtra ("allowedDictionaries", allowedDictionaries);
			this.SetResult (Result.Ok, data);
			Finish ();
		}

		//onBackPressed kan gebruikt worden voor de settings op te slaan bij het teruggaan met de backknop, maar dit leek me eerder onintuitief.
	}
}

