﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Dictionary.services.aonaware.com;


namespace Dictionary
{
	[Activity (Label = "Dictionary", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		private EditText lookupInput;
		private Button lookupButton;
		private ListView definitionsListView;
		private DefinitionsAdapter definitionsAdapter;
		private List<string> allowedDictionaries;
		private List<Definition> allowedDefinitionList;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Main);
			allowedDictionaries = new List<string> ();

			lookupInput = FindViewById<EditText> (Resource.Id.lookupInput);
			lookupButton = FindViewById<Button> (Resource.Id.lookupButton);
			definitionsListView = FindViewById<ListView> (Resource.Id.definitionListView);
			var service = new services.aonaware.com.DictService ();
			if (allowedDictionaries.Count == 0) {
				services.aonaware.com.Dictionary[] dictionaries = service.DictionaryList();
				foreach (services.aonaware.com.Dictionary d in service.DictionaryList()) {
					allowedDictionaries.Add(d.Id);
				}
			}
			lookupButton.Click += delegate {
				Console.WriteLine("definition:" + service.Define(lookupInput.Text).Definitions);
				var definitions = service.Define(lookupInput.Text).Definitions;
				allowedDefinitionList = new  List<Definition>(); 
				if (allowedDictionaries.Count != 0) {
					foreach (Definition d in definitions) {
						if (allowedDictionaries.Contains(d.Dictionary.Id)) {
							allowedDefinitionList.Add(d);
						}
					}
				} else {
					allowedDefinitionList.AddRange(definitions);

				}

				definitionsAdapter = new DefinitionsAdapter (this, allowedDefinitionList.ToArray());
				definitionsListView.Adapter = definitionsAdapter;
			};
				
		}

		public override bool OnCreateOptionsMenu(IMenu menu)
		{
			base.OnCreateOptionsMenu (menu);

			MenuInflater inflater = this.MenuInflater;
			inflater.Inflate (Resource.Menu.menu, menu);
			return true;
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch (item.ItemId)
			{
			case Resource.Id.menu_select_dictionaries:
				//do something
				Console.WriteLine ("Menu selection: select dictionaries");
				var intent = new Intent (this, typeof(DictionarySelectionActivity));
				intent.PutStringArrayListExtra ("allowedDictionaries", allowedDictionaries);
				StartActivityForResult (intent, 1);
				return true;	
			case Resource.Id.menu_share:
				//do something
				Console.WriteLine ("Menu selection: share");
				Intent intent2 = new Intent (Android.Content.Intent.ActionSend);
				//intent2.SetType ("text/html");
				intent2.SetType("message/rfc822");
				intent2.PutExtra(Android.Content.Intent.ExtraEmail, new string[]{"person1@xamarin.com", "person2@xamrin.com"});
				intent2.PutExtra (Android.Content.Intent.ExtraSubject, "Definitions list");
				string definitionsString = "";
				foreach (Definition d in allowedDefinitionList) {
					definitionsString += (d.Dictionary.Name + ": " + d.WordDefinition + "\n");
				}
				intent2.PutExtra(Android.Content.Intent.ExtraText, definitionsString);
				try {
					StartActivity(intent2);
				} catch (ActivityNotFoundException) {
					Toast.MakeText(this, "There are no email clients installed.", ToastLength.Short).Show();
				}

				return true;
			}
			return base.OnOptionsItemSelected(item);
		}

		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);
			Console.WriteLine ("OnActivityResult");
			if (requestCode == 1 && resultCode == Result.Ok) {
				allowedDictionaries = new List<string>(data.GetStringArrayListExtra ("allowedDictionaries"));
			}
		}
	}
}


